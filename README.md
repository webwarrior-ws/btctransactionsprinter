# BTCTransactionsPrinter

Utility for priting incoming transactions for given Bitcoin address.
Currently is a modified geewallet.

# Usage
```
GWallet.Frontend.Console.exe --print-transactions <bitcoin address>
```
