namespace GWallet.Backend.UtxoCoin.Lightning

type internal BrokenChannel =
    {
        ConnectedChannel: ConnectedChannel
    }
